﻿const API_URL = 'https://jsonplaceholder.typicode.com/users';

const xhr = new XMLHttpRequest;

function onRequestHandler() {
    if (this.readyState == 4 && this.status == 200) {


      

        const data = JSON.parse(this.response)


        console.log(data);
        const HTMLResponse = document.querySelector("#app");

        const tpl = data.map((user) => `
            
          <tr>
            <td>${user.id}</th>
            <td>${user.name}</td>
            <td>${user.email}</td>
            
          </tr>`
          
        
        );
        HTMLResponse.innerHTML = `${tpl}`
    }
}


xhr.addEventListener('load', onRequestHandler);
xhr.open('GET', `${API_URL}`);
xhr.send();
