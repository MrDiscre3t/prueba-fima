﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Rodrigo_Gael_Salazar_Morales_8135759321.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

    <!--BOOTSTRAP -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>

   

    <form id="form1" class="container" runat="server">

         <h2>Prueba de Programador</h2> <br />

        <div>


            <!--VALIDACIONES -->

   <asp:RegularExpressionValidator ControlToValidate="TextBox1" ValidationExpression="^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$" ID="RegExRFC" runat="server" ErrorMessage="Verifica tu RFC" hidden>
   </asp:RegularExpressionValidator>
   <asp:RequiredFieldValidator ID="ReqF1" ControlToValidate="TextBox1" runat="server" ErrorMessage="Escribe tu RFC" hidden></asp:RequiredFieldValidator>
   <asp:RequiredFieldValidator ControlToValidate="TextBox2" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Escribe una Razon" hidden></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ControlToValidate="TextBox3" ValidationExpression="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" ID="RegularExpressionValidator1" runat="server" ErrorMessage="La contraseña debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.
NO puede tener otros símbolos" hidden></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ControlToValidate="TextBox3" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Escribe una Contraseña" hidden></asp:RequiredFieldValidator>


            <!--FORMULARIO -->
    
   <div class="container">
  <div class="row">
    <div class="col">
     <label for="exampleFormControlInput1" class="form-label">RFC</label>
     <asp:TextBox  class="form-control"  ID="TextBox1" runat="server"></asp:TextBox>
    </div>
    <div class="col">
        <label for="exampleFormControlInput1" class="form-label">Razon</label>
       <asp:TextBox  class="form-control" ID="TextBox2" runat="server"></asp:TextBox>
    </div>
    <div class="col">
        <label for="exampleFormControlInput1" class="form-label">Contraseña</label>
      <asp:TextBox  class="form-control" ID="TextBox3" runat="server"></asp:TextBox>
    </div>
  </div>
</div>

    <br />

   <asp:Button class="btn btn-primary" ID="Button1" runat="server" Text="Insertar" />

    <asp:Button  class="btn btn-success" ID="Button2" runat="server" Text="Mostrar" />

   <asp:ValidationSummary ID="ValidationSummary1" runat="server" />

    <br />
        </div>
    </form>


    <!--API BACKEND -->

<h2 class="container">Persons from Backend</h2> <br />
    <table class="table container">
            <thead>
                <tr>
             <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Username</th>
            
          </tr>
        </thead>
        <tbody id="app">
        </tbody>
      </table>`

    <script src="index.js" type="module"></script>

</body>
</html>
